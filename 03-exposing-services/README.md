Exposing services
=================

After finishing this course, you should know how to:
* delegate a domain to AWS nameservers
* create an A record and point it to an EC2 instance
* configure TLS to secure your app
* use an application load balancer (with TLS)

## Overview

[Route 53](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/Welcome.html)
is a service to manage your domains and configure them to direct traffic
to other AWS services. Domains can be registered in Route 53 or delegated from other registrars.

[TLS](https://www.cloudflare.com/learning/ssl/transport-layer-security-tls/)
is a popular protocol to secure traffic and verify identity.
Most modern applications use it, even in development environments.
Thanks to services like [Let's Encrypt](https://letsencrypt.org/getting-started/),
which provide free, automatically reneable TLS certificates to anyone,
there are no reasons not to secure your applications in every environment.

Some applications, like the [Caddy](https://hub.docker.com/_/caddy) HTTP server,
integrate with Let's Encrypt and allow to request TLS certificates with very little configuration.

[AWS Certificate Manager](https://docs.aws.amazon.com/acm/latest/userguide/acm-services.html)
integrates with other AWS services, like
[Elastic Load Balancing](https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/what-is-load-balancing.html)
and also allow to use free TLS certificates.


## Excercise

TODO:
1. describe how to delegate a domain: create a hosted zone, get nameservers and configure them in a third-party registrar.
1. run an ec2 instance like in the previous course
1. create an A record for a subdomain, pointing it to the public IP of the ec2 instance
1. start Caddy in a container, on port 443, configuring it to use the subdomain
1. benchmark!

## Test

1. Can an A record point to more than one IP? When this would be useful?
1. If you change the IP to which an A record points to, how fast is the change visible to users around the world?
1. What are common attacks that can be prevented with using TLS?

## Extras

Configure an ELB with multiple instances.
